-- Dia 4
create database familia;

use familia;


CREATE TABLE PAI(
    CODIGO int NOT NULL PRIMARY KEY,
    NOME VARCHAR(255) NOT NULL
);


CREATE TABLE MAE(
    CODIGO int NOT NULL PRIMARY KEY,
    NOME VARCHAR(255) NOT NULL
);

CREATE TABLE FILHO(
    CODIGO int NOT NULL PRIMARY KEY,
    NOME VARCHAR(255) NOT NULL,
    PAI_CODIGO int,
    MAE_CODIGO int,
    FOREIGN KEY (PAI_CODIGO) REFERENCES PAI (CODIGO),
    FOREIGN KEY (MAE_CODIGO) REFERENCES MAE (CODIGO)
);



INSERT INTO MAE (CODIGO, NOME)
VALUES (1, 'Maria');



INSERT INTO MAE (CODIGO, NOME)
VALUES (2, 'Joana');

INSERT INTO MAE (CODIGO, NOME)
VALUES (3, 'Gabriela');


INSERT INTO MAE (CODIGO, NOME)
VALUES (4, 'Juliana');

INSERT INTO MAE (CODIGO, NOME)
VALUES (5, 'Marina');

INSERT INTO PAI (CODIGO, NOME)
VALUES (1, 'Alfredo');

INSERT INTO PAI (CODIGO, NOME)
VALUES (2, 'Joaquim');

INSERT INTO PAI (CODIGO, NOME)
VALUES (3, 'Juca');

INSERT INTO PAI (CODIGO, NOME)
VALUES (4, 'Juliano');

INSERT INTO PAI (CODIGO, NOME)
VALUES (5, 'Moacir');

INSERT INTO FILHO (CODIGO, NOME, PAI_CODIGO, MAE_CODIGO)
VALUES (1, 'Zézinho', 2, 1);

INSERT INTO FILHO (CODIGO, NOME, PAI_CODIGO, MAE_CODIGO)
VALUES (2, 'Tuca', null, 1);

INSERT INTO FILHO (CODIGO, NOME, PAI_CODIGO, MAE_CODIGO)
VALUES (3, 'Jucelino', 3, 1);

INSERT INTO FILHO (CODIGO, NOME, PAI_CODIGO, MAE_CODIGO)
VALUES (4, 'Marcelo', 4, 3);

INSERT INTO FILHO (CODIGO, NOME, PAI_CODIGO, MAE_CODIGO)
VALUES (5, 'Chiquinha', 1, 4);

INSERT INTO FILHO (CODIGO, NOME, PAI_CODIGO, MAE_CODIGO)
VALUES (6, 'Margarida', null, null);

INSERT INTO FILHO (CODIGO, NOME, PAI_CODIGO, MAE_CODIGO)
VALUES (7, 'Rosa', 5, 5);

INSERT INTO FILHO (CODIGO, NOME, PAI_CODIGO, MAE_CODIGO)
VALUES (8, 'Murilo', 1, 4);

INSERT INTO FILHO (CODIGO, NOME, PAI_CODIGO, MAE_CODIGO)
VALUES (9, 'Rodrigo', 2, 2);

INSERT INTO FILHO (CODIGO, NOME, PAI_CODIGO, MAE_CODIGO)
VALUES (10, 'Gustavo', 1, 2);

INSERT INTO FILHO (CODIGO, NOME, PAI_CODIGO, MAE_CODIGO)
VALUES (11, 'Leônidas', 3, 3);

COMMIT;

select * from filho ;

desc mae;

select f.nome,
	   f.codigo_mae
from filho f;

select f.nome,
	   m.nome
from filho f
left join mae m on f.mae_codigo = m.codigo;


select f.nome,
		case when f.mae_codigo is null
				then 'Não tem mae'
				else m.nome end as mae

from filho f
left join mae m on f.mae_codigo = m.codigo;


-- Mostrar os nomes dos filhos, das maes e dos pais
	-- Mostrar somente os filhos que tem mãe ( inner join )
	-- Mostrar os filhos que não tem pai como 'Não tem pai' ( left join )
select f.nome as filho,
	   m.nome as mae,
	   case when f.pai_codigo is null
	   		then 'Não tem pai'
	   		else p.nome end as pai
from filho f
inner join mae m on f.mae_codigo = m.codigo
left join pai p on f.pai_codigo = p.codigo;

-- Mostra o numero de quantos os filhos não tem pai ou mãe 
select count(*)
from filho f
where f.PAI_CODIGO  is null or f.MAE_CODIGO is null; 

-- Mostra os nomes dos coitados
select f.nome
from filho f
where f.PAI_CODIGO  is null or f.MAE_CODIGO is null; 


-- Mostra nome do pai e quantidade de filhos
select p.nome, count(*) as qtde_filhos
from pai p
inner join  filho f on f.pai_codigo = p.codigo
group by p.codigo;

-- Justificativa da informação acima
select *
from pai p
inner join  filho f on f.pai_codigo = p.codigo;

-- Seleciona o nome do filho e da mãe, ordenando pelo nome da mãe 
     -- Alfabeticamente porque eu tenho tempo pra isso 
select m.nome as 'Mãe', f.nome as 'Filho'
from mae m
inner join filho f on f.mae_codigo = m.codigo
order by m.nome asc, f.nome asc;


-- Mostrar o nomes dos filhos que tenham a mãe como Gabriela, Juliana
select  f.nome as 'Filho', m.nome as 'Mãe'
from filho f 
inner join mae m on  m.CODIGO = f.mae_codigo 
where f.mae_codigo = 3 or f.mae_codigo = 4;

-- Codigo da mae 3 e 4



-- Solução mais simples
select f.nome from filho f
where f.mae_codigo = 3 or f.MAE_CODIGO = 4;

-- Com in
select f.nome
from filho f
where f.MAE_CODIGO  in (3,4);

-- Solução com inner join e where com string
select  m.nome as 'Mãe' , f.nome as 'Filho'
from filho f
inner join mae m on m.codigo = f.MAE_CODIGO 
where m.nome = 'Gabriela'
	or m.nome = 'Juliana';

-- Solução com inner join e in
select  m.nome as 'Mãe' , f.nome as 'Filho'
from filho f
inner join mae m on m.codigo = f.MAE_CODIGO 
where f.MAE_CODIGO  in (3,4);

-- Mostrar os filhos que não tem o pai como Juca - 3, Juliano - 4
select f.nome as 'Filho', p.nome as 'Pai'
from filho f
inner join pai p on p.codigo = f.PAI_CODIGO 
where p.codigo not in (3,4)
order by f.nome , p.nome

-- Mostrar os filhos que não tem o pai como Juca - 3, Juliano - 4
select f.nome as 'Filho', p.nome as 'Pai'
from filho f
inner join pai p on p.codigo = f.PAI_CODIGO 
where p.codigo != 3 
	and p.codigo != 4
order by f.nome, p.nome;

-- Mostrar o nome dos Pais que tem mais de 1 filho
select p.nome , count(f.CODIGO) as 'Filhos'
from pai p 
inner join filho f on f.PAI_CODIGO = p.CODIGO 
group by p.CODIGO 
having count(f.CODIGO) > 1;

-- Mostrar pais e mães com mais de um filho
select p.nome as 'Pai' , 
	   m.nome as 'Mãe',
	  count(f.CODIGO) as 'Filhos'
from filho f
inner join pai p on f.PAI_CODIGO = p.CODIGO 
inner join mae m on f.MAE_CODIGO = m.CODIGO 
group by p.CODIGO, m.CODIGO 
having count(f.CODIGO) > 1;

-- Trazer as mães que não tem filho
insert into mae (codigo , nome) values (10, 'mas sem Filho');

select m.*
from mae m
where not exists(select 1 from filho f where m.CODIGO = f.mae_codigo);