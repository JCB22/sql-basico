-- Dia 1 (Aprendemos como usar o Git)

-- Dia 2
create database caderno_compras;

use caderno_compras;

create table compras(
	id 				int not null auto_increment primary key,
	produto 		varchar(50),
	quantidade		int,
	valor			decimal(10,2),
	data_compra		date,
	quem_comprou	varchar(50)
);


insert into compras
(produto, quantidade, valor, data_compra, quem_comprou)
			values
('Cx de Leite', 1, 2.90, now(), 'alicio');
	
    
insert into compras
(produto, quantidade, valor, data_compra, quem_comprou)
			values
('Pão', 10, 12.90, now(), 'dina');

drop table compras;

create table compras(
	id 				int not null auto_increment primary key,
	produto 		varchar(50),
	quantidade		int,
	valor			decimal(10,2),
	data_compra		date,
	quem_comprou	varchar(50)
);

select * from compras;

insert into compras 
(produto, quantidade , valor, data_compra, quem_comprou)
			values
('Cx de Leite', 1, 5.99, now(), 'Alicio Azevedo');

insert into compras 
(produto, quantidade , valor, data_compra, quem_comprou)
			values
('Cx de Leite', 1, 5.99, now(), 'Alicio Azevedo');

insert into compras 
(produto, quantidade , valor, data_compra, quem_comprou)
			values
('Cx de Leite', 1, 5.99, now(), 'Alicio Azevedo');

insert into compras 
(produto, quantidade , valor, data_compra, quem_comprou)
			values
			
('Cx de Leite', 1, 5.99, now(), 'Alicio Azevedo');

delete from compras where id = 3;
-- Uma vez que uma row é deletada, o ID que ela ocupada nunca 
-- mais vai existir

select * from compras;

update compras 
set quem_comprou = 'dina paladina'
where id = 2;

select * from compras;

-- Dia 3

create table produto(
	id int not null auto_increment primary key,
	descricao varchar(100) not null,
	vl_unitario decimal(10,2) default 0.0,
	-- default é o valor caso seja inserido valor nulo no produto
	quantidade_em_estoque int 
);

select * from produto;
select * from compras;

insert into produto (descricao, vl_unitario, quantidade_em_estoque)
			values  ('Caixa de leite', 5.99, 500);
			
		
insert into produto (descricao, vl_unitario, quantidade_em_estoque)
			values  ('Pão com bolinho', 12.99, 100);

insert into produto (descricao, vl_unitario, quantidade_em_estoque)
			values  ('Pão', 0.29, 1000);

insert into produto (descricao, vl_unitario, quantidade_em_estoque)
			values  ('Coca Lata', 2.99, 2000);
		
select * from produto;
		
update produto 
set descricao = 'Coca-Cola Latinha 200 ml'
where id = 4;
			
drop table usuarios;

create table usuarios(
	id int not null auto_increment primary key,
	nome varchar(150) not null,
	cpf varchar(15) not null,
	sexo char not null,
	data_nasc date not null,
	data_cadastro date default now()
);					

insert into usuarios(nome, cpf, sexo, data_nasc)
values   			('Jão', '123.456.789-00', 'M', '2002-02-22');

insert into usuarios(nome, cpf, sexo, data_nasc)
values   			('Torres', '104.966.330-66', 'M', '1992-01-12');

insert into usuarios(nome, cpf, sexo, data_nasc)
values   			('Oliveira', '326.657.520-26', 'F', '1996-08-23');

insert into usuarios(nome, cpf, sexo, data_nasc)
values   			('Teixeira', '751.831.600-58', 'M', '1976-10-29');

insert into usuarios(nome, cpf, sexo, data_nasc)
values   			('Doofenshmirtz', '535.041.280-97', 'F', '1969-05-05');

insert into usuarios(nome, cpf, sexo, data_nasc)
values   			('Matters', '012.347.690-93', 'F', '1979-01-17');

insert into usuarios(nome, cpf, sexo, data_nasc,data_cadastro)
values   			('Jackson', '279.709.930-36', 'F', '1999-04-05','2019-05-30');




select * from usuarios;

select * from compras;

-- Adiciona 'id_usuario para a table compras'
alter table compras add column id_usuario int;

-- Insere as pessoas da compras.quem_comprou para a tabela usuarios
insert into usuarios(nome, cpf, sexo, data_nasc,data_cadastro)
values   			('Alicio Azevedo', '111.111.111-11', 'F', '1999-04-05','2020-05-30');

insert into usuarios(nome, cpf, sexo, data_nasc,data_cadastro)
values   			('dina paladina', '222.222.222-22', 'F', '1989-04-05','2018-05-30');

-- Atualiza o compras.id_usuario baseado no usuarios.id do compras.quem_comprou
update compras 
set id_usuario = 8
where quem_comprou = 'Alicio Azevedo';

update compras 
set id_usuario = 9
where quem_comprou = 'dina paladina';

-- Deleta coluna quem_comprou da tabela de compras
alter table compras drop column quem_comprou;

update compras 
set produto = 'Caixa de Leite'
where produto = 'Cx de Leite';

-- Cria coluna id_produto na tabela compras
alter table compras add column id_produto int;

-- Coloca o id do produto nos respectivos itens
update compras 
set id_produto = 1
where produto = 'Caixa de Leite';

select * from compras;

-- Deleta a coluna produto da tabela compras
alter table compras drop column produto;

select * from compras;
